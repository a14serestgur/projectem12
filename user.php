<?php
class user
{
    private $apikey = '1B321AD51377867CD267DA90BE6CEDA8';// Steam API key
    private $domain = "https://projectem12-sergi111.c9users.io"; // domain web

    public function signIn ()
    {
        require_once 'openid.php';
        $openid = new LightOpenID($this->domain);
        if(!$openid->mode)
        {
            $openid->identity = 'http://steamcommunity.com/openid';
            header('Location: ' . $openid->authUrl());
        }
        elseif($openid->mode == 'cancel')
        {
            print ('User has canceled authentication!');
        }
        else
        {
            if($openid->validate())
            {
                preg_match("/^http:\/\/steamcommunity\.com\/openid\/id\/(7[0-9]{15,25}+)$/", $openid->identity, $matches); // steamID: $matches[1]
                setcookie('steamID', $matches[1], time()+(60*60*24*7), '/'); // 1 week
                header('Location: /');
                
                exit;
            }
            else
            {
                print ('fail');
            }
        }
    }

    public function GetPlayerSummaries ($steamid)
    {
        $response = file_get_contents('http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=' . $this->apikey . '&steamids=' . $steamid);
        //file_put_contents(user.json, $response);
        $json = json_decode($response);
        return $json->response->players[0];
    }
    public function GetGameSummaries ($steamid)
    {
        $playerstats = file_get_contents('http://api.steampowered.com/ISteamUserStats/GetUserStatsForGame/v0002/?appid=730&key=' . $this->apikey . '&steamid=' .  $steamid);
        //file_put_contents(usercsgo.json, $playerstats);
        $json = json_decode($playerstats);
        return $json->playerstats->stats;
    }
    public function GetUserInventory($steamid){
        $url = 'http://api.steampowered.com/IEconItems_730/GetPlayerItems/v0001/?key='.$this->apikey.'&steamid='.$steamid;
        $inventari = file_get_contents($url);
        //file_put_contents(itemsinv.json, $inventari);
        $idsInv = json_decode($inventari);
        $arrayItems = array();
        for($i=0;$i<sizeof($idsInv->result->items);$i++){
            array_push($arrayItems, $idsInv->result->items[$i]->id);
        }
        return $arrayItems;
    }
}
?>