<?php
    require_once 'login.php';
    // Creacio conexio a la base de dades
    $conn = new mysqli($servername, $username, $password, $database);
    // Comprobacio de la conexio
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    
    //Fitxer json dels items
    $allItems = file_get_contents(allItems);
    $it = json_decode($allItems);
    for($i=0;$i<sizeof($it->items);$i++){
        $text = $it->items[$i]->market_name;
        
        //Items amb caracters especials
        if($text == 'M4A4 | 龍王 (Dragon King) (Battle-Scarred)'){
            //Url de la imatge
            $img = $it->items[$i]->icon_url;
            //Insercio per StatTrak = TRUE
            $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
            VALUES ('Rifle','M4A4','Dragon King','Battle-Scarred', '$img', TRUE )";
            
            if ($conn->query($sql) === TRUE) {
                echo "New record created successfully";
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
            
            //Insercio per StatTrak = FALSE
            $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
            VALUES ('Rifle','M4A4','Dragon King','Battle-Scarred','$img',FALSE )";
            
            if ($conn->query($sql) === TRUE) {
                echo "New record created successfully";
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
        }
        
        elseif($text == 'M4A4 | 龍王 (Dragon King) (Factory New)'){
            //Url de la imatge
            $img = $it->items[$i]->icon_url;
            //Insercio per StatTrak = TRUE
            $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
            VALUES ('Rifle','M4A4','Dragon King','Factory New', '$img', TRUE )";
            
            if ($conn->query($sql) === TRUE) {
                echo "New record created successfully";
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
            
            //Insercio per StatTrak = FALSE
            $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
            VALUES ('Rifle','M4A4','Dragon King','Factory New','$img',FALSE )";
            
            if ($conn->query($sql) === TRUE) {
                echo "New record created successfully";
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }    
        }
        
        
        elseif($text == 'M4A4 | 龍王 (Dragon King) (Field-Tested)'){
            //Url de la imatge
            $img = $it->items[$i]->icon_url;
            //Insercio per StatTrak = TRUE
            $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
            VALUES ('Rifle','M4A4','Dragon King','Field-Tested', '$img', TRUE )";
            
            if ($conn->query($sql) === TRUE) {
                echo "New record created successfully";
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
            
            //Insercio per StatTrak = FALSE
            $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
            VALUES ('Rifle','M4A4','Dragon King','Field-Tested','$img',FALSE )";
            
            if ($conn->query($sql) === TRUE) {
                echo "New record created successfully";
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }    
        }
        
        
        elseif($text == 'M4A4 | 龍王 (Dragon King) (Minimal Wear)'){
            //Url de la imatge
            $img = $it->items[$i]->icon_url;
            //Insercio per StatTrak = TRUE
            $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
            VALUES ('Rifle','M4A4','Dragon King','Minimal Wear', '$img', TRUE )";
            
            if ($conn->query($sql) === TRUE) {
                echo "New record created successfully";
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
            
            //Insercio per StatTrak = FALSE
            $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
            VALUES ('Rifle','M4A4','Dragon King','Minimal Wear','$img',FALSE )";
            
            if ($conn->query($sql) === TRUE) {
                echo "New record created successfully";
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
        }
        
        
        elseif($text == 'M4A4 | 龍王 (Dragon King) (Well-Worn)'){
            //Url de la imatge
            $img = $it->items[$i]->icon_url;
            //Insercio per StatTrak = TRUE
            $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
            VALUES ('Rifle','M4A4','Dragon King','Well-Worn', '$img', TRUE )";
            
            if ($conn->query($sql) === TRUE) {
                echo "New record created successfully";
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
            
            //Insercio per StatTrak = FALSE
            $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
            VALUES ('Rifle','M4A4','Dragon King','Well-Worn','$img',FALSE )";
            
            if ($conn->query($sql) === TRUE) {
                echo "New record created successfully";
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }    
        }
        
        else{
            $nueva_cadena = ereg_replace("[★]", "", $text);
            list($tipus, $segon) = split('[|]', $nueva_cadena);
            $tipus = trim($tipus);
            list($skin, $tercer) = split('[(]', $segon);
            $skin = trim($skin);
            list($exterior) = split('[)]', $tercer);
            $exterior = trim($exterior);
            //Comprobacio per cada item i insercio
            if ($tipus == 'Bayonet' && $skin != NULL){
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Knife','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Knife','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif($tipus == 'Bowie Knife' && $skin != NULL){
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Knife','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Knife','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif($tipus == 'Butterfly Knife' && $skin != NULL){
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Knife','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Knife','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif($tipus == 'Falchion Knife' && $skin != NULL){
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Knife','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Knife','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif($tipus == 'Flip Knife' && $skin != NULL){
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Knife','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Knife','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif($tipus == 'Gut Knife' && $skin != NULL){
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Knife','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Knife','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif($tipus == 'Huntsman Knife' && $skin != NULL){
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Knife','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Knife','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif($tipus == 'Karambit' && $skin != NULL){
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Knife','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Knife','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif($tipus == 'M9 Bayonet' && $skin != NULL){
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Knife','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Knife','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif($tipus == 'Shadow Daggers' && $skin != NULL){
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Knife','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Knife','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif($tipus == 'AK-47'){
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Rifle','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Rifle','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif ($tipus == 'AUG') {
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Rifle','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Rifle','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif ($tipus == 'AWP') {
                //Url de la imatge
                //$img = $it->items[$i]->icon_url;
                
                
                if ($skin == 'Man-o\'-war'){
                    $skin = "Man-o\'-war";
                }
                
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Sniper Rifle','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Sniper Rifle','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif ($tipus == 'CZ75-Auto') {
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Pistol','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Pistol','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif ($tipus == 'Desert Eagle') {
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Pistol','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Pistol','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif ($tipus == 'Dual Berettas') {
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Pistol','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Pistol','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif ($tipus == 'FAMAS') {
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Rifle','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Rifle','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif ($tipus == 'Five-SeveN') {
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Pistol','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Pistol','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif ($tipus == 'G3SG1') {
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Sniper Rifle','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Sniper Rifle','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif ($tipus == 'Galil AR') {
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Rifle','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Rifle','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif ($tipus == 'Glock-18') {
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Pistol','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Pistol','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif ($tipus == 'M249') {
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Machinegun','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Machinegun','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif ($tipus == 'M4A1-S') {
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Rifle','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Rifle','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            if ($tipus == 'M4A4') {
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Rifle','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Rifle','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif ($tipus == 'MAC-10') {
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('SMG','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('SMG','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif ($tipus == 'MAG-7') {
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Shotgun','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Shotgun','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif ($tipus == 'MP7') {
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('SMG','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('SMG','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            if ($tipus == 'MP9') {
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                if($skin == "Pandora's Box"){
                    $skin = "Pandora\'s Box";
                }
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('SMG','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('SMG','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif ($tipus == 'Negev') {
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                if ($skin == 'Man-o\'-war'){
                    $skin = "Man-o\'-war";
                }
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Machinegun','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Machinegun','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif ($tipus == 'Nova') {
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Shotgun','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Shotgun','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif ($tipus == 'P2000') {
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Pistol','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Pistol','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif ($tipus == 'P250') {
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Pistol','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Pistol','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif ($tipus == 'P90') {
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('SMG','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('SMG','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif ($tipus == 'PP-Bizon') {
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('SMG','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('SMG','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif ($tipus == 'R8 Revolver') {
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Pistol','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Pistol','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif ($tipus == 'Sawed-Off') {
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Shotgun','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Shotgun','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif ($tipus == 'SCAR-20') {
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Sniper Rifle','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Sniper Rifle','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            
            
            elseif ($tipus == 'SG 553') {
                //Url de la imatge
                $img = $it->items[$i]->icon_url;
                
                //Insercio per StatTrak = TRUE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Rifle','$tipus', '$skin', '$exterior', '$img', TRUE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                
                //Insercio per StatTrak = FALSE
                $sql = "INSERT INTO items (tipus, name, skin, exterior, img, StatTrak)
                VALUES ('Rifle','$tipus', '$skin', '$exterior', '$img', FALSE )";
                
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
        }
    }
    $conn->close();
?>