CREATE DATABASE projectem12;

CREATE TABLE users(id INT auto_increment, steamid INT, tradelink varchar(255), PRIMARY KEY(id));

CREATE TABLE exterior(id INT NOT NULL auto_increment, name VARCHAR(255) UNIQUE, PRIMARY KEY (id));

INSERT INTO exterior(name) VALUES("Factory New"), ("Minimal Wear"),("Field-Tested"),("Well-Worn"),("Battle-Scarred");

CREATE TABLE tipus(id INT NOT NULL auto_increment, name VARCHAR(255) UNIQUE, PRIMARY KEY (id));

INSERT INTO tipus(name) VALUES ("Knife"), ("Pistol"), ("SMG"),("Shotgun"),("Rifle"),("Sniper Rifle"),("Machinegun");

CREATE TABLE items(id INT NOT NULL auto_increment, tipus VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, skin VARCHAR(255) NOT NULL, exterior VARCHAR(255) NOT NULL,
img VARCHAR(255),PRIMARY KEY(id));

ALTER TABLE items ADD FOREIGN KEY (tipus) REFERENCES tipus(name);

ALTER TABLE items ADD FOREIGN KEY (exterior) REFERENCES exterior(name);

CREATE TABLE name(id INT NOT NULL auto_increment, name VARCHAR(255) UNIQUE, PRIMARY KEY (id));

INSERT INTO name(name) VALUES ('Bayonet'),('Bowie Knife'),('Butterfly Knife'),('Falchion Knife'),('Flip Knife'),('Gut Knife'),('Huntsman Knife'),('Karambit'),('M9 Bayonet'),('Shadow Daggers'),('AK-47'),('AUG'),('AWP'),('CZ75-Auto'),('Desert Eagle'),('Dual Berettas'),('FAMAS'),('Five-SeveN'),('G3SG1'),('Galil AR'),('Glock-18'),('M249'),('M4A1-S'),('M4A4'),('MAC-10'),('MAG-7'),('MP7'),('MP9'),('Negev'),('Nova'),('P2000'),('P250'),('P90'),('PP-Bizon'),('R8 Revolver'),('Sawed-Off'),('SCAR-20'),('SG 553');

ALTER TABLE items ADD FOREIGN KEY (name) REFERENCES name(name);

CREATE TABLE trades(id INT AUTO_INCREMENT, userid INT, itemsUser varchar(255), itemsWeb varchar(255), PRIMARY KEY(id));