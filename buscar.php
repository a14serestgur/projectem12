<?php
    require_once 'login.php';
    
    //cantitat de registres a mostrar.
    $registros = 50;
 
    $contador = 1;
    
    $tipus = $_GET['tipus'];
    $nom = $_GET['nom'];
    $exterior = $_GET['exterior'];
    $pagina = $_GET["pagina"];
    $trobat = null;
    $trobat = "<div class='row' id='itemsBuscats'>";
    
    // Creacio conexio a la base de dades
    $conn = new mysqli($servername, $username, $password, $database);
    // Comprobacio de la conexio
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    
    /**
     * S'inicia la paginacio, si el valor de $pagina es 0 s'asigna el valor 1 i $inicio entra amb valor 0.
     * si no es la pagina 1, $inicio sera igual al numero de pagina menys 1 multiplicat per la cantitat de registres
     */
    if (!$pagina) { 
        $inicio = 0; 
        $pagina = 1; 
    } else { 
        $inicio = ($pagina - 1) * $registros; 
    } 

    if($tipus == 'all' && $nom != 'all' && $exterior != 'all'){
        		
    	$sql = 'SELECT * FROM items WHERE name="'.$nom.'" && exterior="'.$exterior.'"';
		        
	}elseif( $tipus != 'all' && $nom == 'all' && $exterior != 'all'){
		        
	    $sql = 'SELECT * FROM items WHERE tipus="'.$tipus.'" && exterior="'.$exterior.'"';
		        
	}elseif( $tipus != 'all' && $nom != 'all' && $exterior == 'all'){
		        
		$sql = 'SELECT * FROM items WHERE tipus="'.$tipus.'" && name="'.$nom.'"';
		        
	}elseif( $tipus == 'all' && $nom == 'all' && $exterior != 'all'){
		        
	    $sql = 'SELECT * FROM items WHERE exterior="'.$exterior.'"';
		        
    }elseif( $tipus == 'all' && $nom != 'all' && $exterior == 'all'){
		        
        $sql = 'SELECT * FROM items WHERE tipus="'.$tipus.'" && exterior="'.$exterior.'"';
		        
	}elseif( $tipus != 'all' && $nom == 'all' && $exterior == 'all'){
		        
		$sql = 'SELECT * FROM items WHERE tipus="'.$tipus.'"';
		        
	}elseif($tipus != 'all' && $nom != 'all' && $exterior != 'all'){
		        
		$sql = 'SELECT * FROM items WHERE tipus="'.$tipus.'" && name="'.$nom.'" && exterior="'.$exterior.'"';
		        
	}else{
		        
		$sql = 'SELECT * FROM items';
		        
	}
		    
		$resultados = mysqli_query($conn,$sql);
		    
		$total_registros = mysqli_num_rows($resultados);
        		
        $sql .= " LIMIT $inicio, $registros";
        		
        $resultados = mysqli_query($conn,$sql);
    
        //Amb ceil arrodonim el resultat total de las paginas
		$total_paginas = ceil($total_registros / $registros); 	
    
        if ($total_registros) {
			while ($items = mysqli_fetch_array($resultados, MYSQLI_ASSOC)) {
              if($items["StatTrak"] == 1){
                    $trobat = $trobat."<div id='".$items["id"]."' class='col-md-3 item stattrak' onclick='clickItemWeb(this.id)'>" . $items["name"]."<br><img src=".$items["img"]."><br>".$items["skin"]. "<br>".$items["exterior"]."<br><p>Stattrak: True</p></div>";
                }else{
                    $trobat = $trobat."<div id='".$items["id"]."' class='col-md-3 item' onclick='clickItemWeb(this.id)'> " . $items["name"]."<br><img src=".$items["img"]."><br>".$items["skin"]. "<br>".$items["exterior"]."<br><p>Stattrak: False</p></div>";
                }
			}
            $trobat .='</div>';
	        
		
			/**
			 * Acá activamos o desactivamos la opción "< Anterior", si estamos en la pagina 1 nos dará como resultado 0 por ende NO
			 * activaremos el primer if y pasaremos al else en donde se desactiva la opción anterior. Pero si el resultado es mayor
			 * a 0 se activara el href del link para poder retroceder.
			 */
			$trobat .= "<nav><ul class='pagination'>"; 
 
			// Generamos el ciclo para mostrar la cantidad de paginas que tenemos.
			for ($i = 1; $i <= $total_paginas; $i++) {
				if ($pagina == $i) {
					$trobat .= "<li><a href='#'>". $pagina ."</a></li>"; 
				} else {
					$func = '';
					$func .= 'FAjax(\'buscar.php\',\'items\',\'tipus='.$tipus.'&nom='.$nom.'&exterior='.$exterior.'&pagina='.$i.'\',\'POST\')';
					$trobat .= '<li><a href="#" onclick="'.$func.'">'.$i.'</a></li>'; 
				}	
			}
 
        } else {
        	$trobat .= "<font color='darkgray'>(sin resultados)</font>";
        }
			$trobat .= "</ul></nav>";
		
    
    $conn->close();
    echo $trobat;
?>