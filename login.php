<?php
    $servername = getenv('IP');
    $username = getenv('C9_USER');
    $password = "";
    $database = "projectem12";
    $dbport = 3306;
    
    //Conectar al Servidor
    $conn = new mysqli($servername, $username, $password, $database);
     
    //Verificacio Conexión
    if (mysqli_connect_errno()) {
    	exit('Fallo Conexion: '. mysqli_connect_error());
    }
?>