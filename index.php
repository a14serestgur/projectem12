<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>CSGO Trades</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="theme.css">
    <script type="text/javascript" src="index.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <link rel="shortcut icon" type="image/ico" href="img/Iconos/favicon.ico"/>
</head>
<body>
<?php
error_reporting(E_ERROR | E_PARSE | E_WARNING);
require_once 'user.php';
require_once 'functions.php';
require_once 'login.php';
$user = new user;
$steamid= '76561198007213978';
    //Insercio User Base Dades
    
    // Creacio conexio a la base de dades
    $conn = new mysqli($servername, $username, $password, $database);
    // Comprobacio de la conexio
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    
    $sql = 'SELECT * FROM users WHERE steamid="'.$steamid.'"';
    
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
    }else{
        
        $sql = 'INSERT INTO users (steamid) VALUES ("'.$steamid.'")';
        
        if ($conn->query($sql) === TRUE) {
        echo "New record created successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }

    $conn->close();
    }
    
    //Menu
    echo '<br><center><div class="logo"><img id="logo" src="img/logoCSGOTrades.png" alt="logo"></div></center><br>
    <nav class="navbar navbar-default">
            <ul class="nav nav-pills">
                <li role="presentation" id="presentation" class="active"><a href="index.php">Home</a></li>
                <li role="presentation" id="presentation"><a href="newTrade.php">New Trade</a></li>
                <li role="presentation" id="presentation"><a href="searchTrades.php">Search</a></li>
                <li role="presentation" id="presentation"><a href="searchAllTrades.php">Search All</a></li>
                <li role="presentation" id="presentation"><a href="profile.php">Profile</a></li>
            </ul>
        </nav>
        <div class="container">
            <div class="blog-header">
                <h1 class="blog-title"><center>Steam Account</center></h1>
            </div>
            <div class="row rowAccount">
                <div class="col-md-6">
                    <center><img src='.$user->GetPlayerSummaries($steamid)->avatarfull.'/></center>
                </div>
                <div class="col-md-6">
                    <div class="statsDiv">';
                    $co = $user->GetPlayerSummaries($steamid)->loccountrycode;
                    $co = strtolower($co);
                    echo'<center><table><tr><td>Real Name: '.$user->GetPlayerSummaries($steamid)->realname.'</td></tr><tr><td>
                        Name: '.$user->GetPlayerSummaries($steamid)->personaname.'</td></tr><tr><td>
                        Steam Id: '.$user->GetPlayerSummaries($steamid)->steamid.'</td></tr><tr><td>
                        Url Profile: '.$user->GetPlayerSummaries($steamid)->profileurl.'</td></tr><tr><td>
                        Country: '.$user->GetPlayerSummaries($steamid)->loccountrycode.' | <img src="http://cdn.steamcommunity.com/public/images/countryflags/'.$co.'.gif" alt=""> |</td></tr></table></center>
                    </div>
                </div>
            </div>
            <br>
            <div class="row rowAccount">
                <center><h1>General Statistics</h1></center>
            </div>
            <div class="row rowAccount">
                <div class="col-md-8">';
                    $headShots = porcentaje($user->GetGameSummaries($steamid)[24]->value,$user->GetGameSummaries($steamid)[0]->value,1);
                    $kdRatio = kdRatio($user->GetGameSummaries($steamid)[0]->value,$user->GetGameSummaries($steamid)[1]->value,2);
                    echo '<table width="100%"> 
                        <tr>
                            <td align="center" class="tdStats"><img src="../img/Iconos/kills.png" alt="Kills"><br>'. $user->GetGameSummaries($steamid)[0]->value.'<br>Kills</td>
                            <td align="center" class="tdStats"><img src="../img/Iconos/death.png" alt="Death"><br>'. $user->GetGameSummaries($steamid)[1]->value.'<br>Deaths</td>
                            <td align="center" class="tdStats"><img src="../img/Iconos/HeadShot.png" alt="headshot" width="64px" height="64px"><br>'. $headShots.'%<br>Headshots</td>
                            <td align="center" class="tdStats"><img src="../img/Iconos/kdratio.ico" alt="k/D" width="64px" height="64px"><br>'. $kdRatio.'<br>K/D Ratio</td>
                        </tr>
                        <tr>
                            <td align="center" class="tdStats"><img src="../img/Iconos/star.png" alt="mvp" width="64px" height="64px"><br>'. $user->GetGameSummaries($steamid)[101]->value. '<br>Total MVP\'s</td>
                            <td align="center" class="tdStats"><img src="../img/Iconos/totalDamage.png" alt="damage" width="64px" height="64px"><br>'. $user->GetGameSummaries($steamid)[6]->value.'<br>Total Damage Done</td>
                            <td align="center" class="tdStats"><img src="../img/Iconos/dinero.png" alt="Money"><br>'. $user->GetGameSummaries($steamid)[7]->value.'$<br>Total Money Earned</td>
                            <td align="center" class="tdStats"><img src="../img/Iconos/hitsShot.png" alt="shots" width="64px" height="64px"><br>'. $user->GetGameSummaries($steamid)[45]->value.'<br>Total Shots Hit</td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-4">
                    <table width="100%">
                        <tr>
                            <td align="center" class="tdStats"><img src="../img/Iconos/win.png" alt="Win"><br>'. $user->GetGameSummaries($steamid)[5]->value.'<br>Wins</td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td align="center" class="tdStats"><img src="../img/Iconos/bomb.png" alt="bomb" width="64px" height="64px"><br>'. $user->GetGameSummaries($steamid)[3]->value.'<br>Planted Bombs</td>
                            <td align="center" class="tdStats"><img src="../img/Iconos/defuse.png" alt="defuse" width="64px" height="64px"><br>'. $user->GetGameSummaries($steamid)[4]->value.'<br>Defused Bombs</td>
                        </tr>
                    </table>
                </div>
            </div>
            <br>
            <div class="row rowAccount">
                <center><h1>CT Weapons Kills Statistics</h1></center>
            </div>
            <div class="row rowAccount">';
            
                //Estadisticas Kills Armas
                $contentStatsKillsCt = '';
                $stats = $user->GetGameSummaries($steamid);
                
                for($i=0;$i<sizeof($stats);$i++){
                    if($stats[$i]->name == 'total_kills_knife'){
                        $contentStatsKillsCt = $contentStatsKillsCt.'<div class="col-md-2"><center><img src="../img/Weapons_stats/knife.png" alt="Knife" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>Knife Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_hegrenade'){
                        $contentStatsKillsCt = $contentStatsKillsCt.'<div class="col-md-2"><center><img src="../img/Weapons_stats/hegrenade.png" alt="hegrenade" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>Hegrenade Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_deagle'){
                        $contentStatsKillsCt = $contentStatsKillsCt.'<div class="col-md-2"><center><img src="../img/Weapons_stats/deagle.png" alt="deagle" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>Deagle Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_elite'){
                        $contentStatsKillsCt = $contentStatsKillsCt.'<div class="col-md-2"><center><img src="../img/Weapons_stats/elite.png" alt="elite" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>Elite Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_fiveseven'){
                        $contentStatsKillsCt = $contentStatsKillsCt.'<div class="col-md-2"><center><img src="../img/Weapons_stats/fiveseven.png" alt="fiveseven" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>Fiveseven Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_xm1014'){
                        $contentStatsKillsCt = $contentStatsKillsCt.'<div class="col-md-2"><center><img src="../img/Weapons_stats/xm1014.png" alt="xm1014" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>Xm1014 Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_mac10'){
                        $contentStatsKillsCt = $contentStatsKillsCt.'<div class="col-md-2"><center><img src="../img/Weapons_stats/mac10.png" alt="mac10" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>Mac10 Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_ump45'){
                        $contentStatsKillsCt = $contentStatsKillsCt.'<div class="col-md-2"><center><img src="../img/Weapons_stats/ump45.png" alt="ump45" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>Ump45 Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_p90'){
                        $contentStatsKillsCt = $contentStatsKillsCt.'<div class="col-md-2"><center><img src="../img/Weapons_stats/p90.png" alt="p90" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>P90 Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_awp'){
                        $contentStatsKillsCt = $contentStatsKillsCt.'<div class="col-md-2"><center><img src="../img/Weapons_stats/awp.png" alt="awp" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>AWP Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_aug'){
                        $contentStatsKillsCt = $contentStatsKillsCt.'<div class="col-md-2"><center><img src="../img/Weapons_stats/aug.png" alt="aug" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>AUG Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_famas'){
                        $contentStatsKillsCt = $contentStatsKillsCt.'<div class="col-md-2"><center><img src="../img/Weapons_stats/famas.png" alt="famas" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>Famas Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_m249'){
                        $contentStatsKillsCt = $contentStatsKillsCt.'<div class="col-md-2"><center><img src="../img/Weapons_stats/m249.png" alt="m249" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>M249 Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_hkp2000'){
                        $contentStatsKillsCt = $contentStatsKillsCt.'<div class="col-md-2"><center><img src="../img/Weapons_stats/hkp2000.png" alt="hkp2000" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>P2000 Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_p250'){
                        $contentStatsKillsCt = $contentStatsKillsCt.'<div class="col-md-2"><center><img src="../img/Weapons_stats/p250.png" alt="p250" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>P250 Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_scar20'){
                        $contentStatsKillsCt = $contentStatsKillsCt.'<div class="col-md-2"><center><img src="../img/Weapons_stats/scar20.png" alt="scar20" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>SCAR20 Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_ssg08'){
                        $contentStatsKillsCt = $contentStatsKillsCt.'<div class="col-md-2"><center><img src="../img/Weapons_stats/ssg08.png" alt="ssg08" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>SSG08 Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_mp7'){
                        $contentStatsKillsCt = $contentStatsKillsCt.'<div class="col-md-2"><center><img src="../img/Weapons_stats/mp7.png" alt="mp7" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>MP7 Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_mp9'){
                        $contentStatsKillsCt = $contentStatsKillsCt.'<div class="col-md-2"><center><img src="../img/Weapons_stats/mp9.png" alt="mp9" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>MP9 Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_nova'){
                        $contentStatsKillsCt = $contentStatsKillsCt.'<div class="col-md-2"><center><img src="../img/Weapons_stats/nova.png" alt="nova" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>Nova Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_negev'){
                        $contentStatsKillsCt = $contentStatsKillsCt.'<div class="col-md-2"><center><img src="../img/Weapons_stats/negev.png" alt="negev" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>Negev Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_bizon'){
                        $contentStatsKillsCt = $contentStatsKillsCt.'<div class="col-md-2"><center><img src="../img/Weapons_stats/bizon.png" alt="bizon" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>Bizon Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_mag7'){
                        $contentStatsKillsCt = $contentStatsKillsCt.'<div class="col-md-2"><center><img src="../img/Weapons_stats/mag7.png" alt="mag7" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>Mag7 Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_m4a1'){
                        $contentStatsKillsCt = $contentStatsKillsCt.'<div class="col-md-2"><center><img src="../img/Weapons_stats/m4a1.png" alt="m4a1" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>M4A1 Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_molotov'){
                        $contentStatsKillsCt = $contentStatsKillsCt.'<div class="col-md-2"><center><img src="../img/Weapons_stats/molotov.png" alt="molotov" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>Molotov Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_taser'){
                        $contentStatsKillsCt = $contentStatsKillsCt.'<div class="col-md-2"><center><img src="../img/Weapons_stats/taser.png" alt="taser" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>Taser Kills</center></div>';
                    }
                }
            
        echo $contentStatsKillsCt;
            
        echo'</div>
            <br>
            <hr>
            <br>
            <div class="row rowAccount">
                <center><h1>T Weapons Kills Statistics</h1></center>
            </div>
            <div class="row rowAccount">';
                //Estadisticas Kills Armas
                $contentStatsKillsT = '';
                $stats = $user->GetGameSummaries($steamid);
                
                for($i=0;$i<sizeof($stats);$i++){
                    if($stats[$i]->name == 'total_kills_knife'){
                        $contentStatsKillsT = $contentStatsKillsT.'<div class="col-md-2"><center><img src="../img/Weapons_stats/knife.png" alt="Knife" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>Knife Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_hegrenade'){
                        $contentStatsKillsT = $contentStatsKillsT.'<div class="col-md-2"><center><img src="../img/Weapons_stats/hegrenade.png" alt="hegrenade" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>Hegrenade Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_glock'){
                        $contentStatsKillsT = $contentStatsKillsT.'<div class="col-md-2"><center><img src="../img/Weapons_stats/glock.png" alt="glock" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>Glock Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_deagle'){
                        $contentStatsKillsT = $contentStatsKillsT.'<div class="col-md-2"><center><img src="../img/Weapons_stats/deagle.png" alt="deagle" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>Deagle Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_elite'){
                        $contentStatsKillsT = $contentStatsKillsT.'<div class="col-md-2"><center><img src="../img/Weapons_stats/elite.png" alt="elite" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>Elite Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_xm1014'){
                        $contentStatsKillsT = $contentStatsKillsT.'<div class="col-md-2"><center><img src="../img/Weapons_stats/xm1014.png" alt="xm1014" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>Xm1014 Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_mac10'){
                        $contentStatsKillsT = $contentStatsKillsT.'<div class="col-md-2"><center><img src="../img/Weapons_stats/mac10.png" alt="mac10" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>Mac10 Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_ump45'){
                        $contentStatsKillsT = $contentStatsKillsT.'<div class="col-md-2"><center><img src="../img/Weapons_stats/ump45.png" alt="ump45" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>Ump45 Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_p90'){
                        $contentStatsKillsT = $contentStatsKillsT.'<div class="col-md-2"><center><img src="../img/Weapons_stats/p90.png" alt="p90" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>P90 Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_awp'){
                        $contentStatsKillsT = $contentStatsKillsT.'<div class="col-md-2"><center><img src="../img/Weapons_stats/awp.png" alt="awp" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>AWP Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_ak47'){
                        $contentStatsKillsT = $contentStatsKillsT.'<div class="col-md-2"><center><img src="../img/Weapons_stats/ak47.png" alt="ak47" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>AK47 Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_g3sg1'){
                        $contentStatsKillsT = $contentStatsKillsT.'<div class="col-md-2"><center><img src="../img/Weapons_stats/g3sg1.png" alt="g3sg1" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>G3SG1 Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_m249'){
                        $contentStatsKillsT = $contentStatsKillsT.'<div class="col-md-2"><center><img src="../img/Weapons_stats/m249.png" alt="m249" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>M249 Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_p250'){
                        $contentStatsKillsT = $contentStatsKillsT.'<div class="col-md-2"><center><img src="../img/Weapons_stats/p250.png" alt="p250" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>P250 Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_sg556'){
                        $contentStatsKillsT = $contentStatsKillsT.'<div class="col-md-2"><center><img src="../img/Weapons_stats/sg556.png" alt="sg556" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>SG556 Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_ssg08'){
                        $contentStatsKillsT = $contentStatsKillsT.'<div class="col-md-2"><center><img src="../img/Weapons_stats/ssg08.png" alt="ssg08" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>SSG08 Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_mp7'){
                        $contentStatsKillsT = $contentStatsKillsT.'<div class="col-md-2"><center><img src="../img/Weapons_stats/mp7.png" alt="mp7" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>MP7 Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_mp9'){
                        $contentStatsKillsT = $contentStatsKillsT.'<div class="col-md-2"><center><img src="../img/Weapons_stats/mp9.png" alt="mp9" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>MP9 Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_nova'){
                        $contentStatsKillsT = $contentStatsKillsT.'<div class="col-md-2"><center><img src="../img/Weapons_stats/nova.png" alt="nova" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>Nova Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_negev'){
                        $contentStatsKillsT = $contentStatsKillsT.'<div class="col-md-2"><center><img src="../img/Weapons_stats/negev.png" alt="negev" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>Negev Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_sawedoff'){
                        $contentStatsKillsT = $contentStatsKillsT.'<div class="col-md-2"><center><img src="../img/Weapons_stats/sawedoff.png" alt="sawedoff" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>Sawedoff Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_bizon'){
                        $contentStatsKillsT = $contentStatsKillsT.'<div class="col-md-2"><center><img src="../img/Weapons_stats/bizon.png" alt="bizon" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>Bizon Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_tec9'){
                        $contentStatsKillsT = $contentStatsKillsT.'<div class="col-md-2"><center><img src="../img/Weapons_stats/tec9.png" alt="tec9" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>Tec9 Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_galilar'){
                        $contentStatsKillsT = $contentStatsKillsT.'<div class="col-md-2"><center><img src="../img/Weapons_stats/galilar.png" alt="galilar" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>Galil ar Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_molotov'){
                        $contentStatsKillsT = $contentStatsKillsT.'<div class="col-md-2"><center><img src="../img/Weapons_stats/molotov.png" alt="molotov" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>Molotov Kills</center></div>';
                    }
                    if($stats[$i]->name == 'total_kills_taser'){
                        $contentStatsKillsT = $contentStatsKillsT.'<div class="col-md-2"><center><img src="../img/Weapons_stats/taser.png" alt="taser" width="100%">'.$user->GetGameSummaries($steamid)[$i]->value.'<br>Taser Kills</center></div>';
                    }
                }
            
        echo $contentStatsKillsT;
        
        echo '</div>    
        </div>';
?>
</body>
</html>


