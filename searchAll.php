<?php
    require_once 'login.php';
    require_once 'user.php';
    $user = new user;

    // Creacio conexio a la base de dades
    $conn = new mysqli($servername, $username, $password, $database);
    // Comprobacio de la conexio
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
                   
    $trobat = '';
                
    $sql = 'SELECT * FROM trades';
                            
    $result = $conn->query($sql);
                    
    if ($result->num_rows > 0) {
    // output data of each row
        while($row = $result->fetch_assoc()) {
                                
                //Busqueda del Trade Link
                $sqlUser = 'SELECT steamid, tradelink FROM users WHERE steamid='.$row['userid'];
                $resultUser = $conn->query($sqlUser); 
                if ($resultUser->num_rows > 0) {    
                    while($rowUser = $resultUser->fetch_assoc()) {
                        $tradeLink = $rowUser['tradelink'];
                        $steamid = $rowUser['steamid'];
                    }    
                
                //Mostrar Items Web Oferta
                $itemsWeb = explode(",", $row['itemsWeb']);
                $mostraItemsWeb = "";
                for($i=0;$i<sizeof($itemsWeb);$i++){
                    $sqlMostrarItemsWeb = 'SELECT name,skin,img,StatTrak,exterior FROM items WHERE id='.$itemsWeb[$i];
                    $resultMostrarItemsWeb = $conn->query($sqlMostrarItemsWeb);
                    if ($resultMostrarItemsWeb->num_rows > 0) {
                        // output data of each row
                        while($rowMostrarItemsWeb = $resultMostrarItemsWeb->fetch_assoc()) {
                            $mostraItemsWeb = $mostraItemsWeb.'<div class="item">'.$rowMostrarItemsWeb['name'].'<br><img src="'.$rowMostrarItemsWeb['img'].'"><br>'.$rowMostrarItemsWeb['skin'].'<br>'.$rowMostrarItemsWeb['exterior'].'</div>';       
                        }
                    }
                }
                //Mostrar Items User Oferta
                //echo $row['itemsUser'];
                $itemsUser = explode(",",$row['itemsUser']);
                $mostraItemsUser = "";
                $items = file_get_contents('http://steamcommunity.com/profiles/'.$steamid.'/inventory/json/730/2');
                $item = json_decode($items);
                for($i=0;$i<sizeof($itemsUser);$i++){
                    $nomItem = $item->rgDescriptions->$itemsUser[$i]->market_name;
                    $imgItemUser = 'http://steamcommunity-a.akamaihd.net/economy/image/'.$item->rgDescriptions->$itemsUser[$i]->icon_url;
                    $mostraItemsUser = $mostraItemsUser.'<div class="item">'.$nomItem.'<br><img src="'.$imgItemUser.'"></div>';
                }
                
                //Imprimeix el resultat de cada oferta
                $idBuscats = $idBuscats.'Id: '.$row['id'];
                $trobat = $trobat.'<div class="row"><div class="col-md-10 col-md-offset-1 trobat"><div class="row"><div class="col-md-12" id="idTrade">User: '.$user->GetPlayerSummaries($row['userid'])->personaname.'</div></div><div class="row"><div class="col-md-12" id="resultatItems"><div class="row"><div class="col-md-5" id="leftUser">'.$mostraItemsUser.'</div><div class="col-md-2"></div><div class="col-md-5" id="rightWeb">'.$mostraItemsWeb.'</div></div></div></div><div id="tradeLink"><button id="buttonTrade"><a href="'.$tradeLink.'">Trade</a></button></div></div></div>';
            }
        }
    }
    if($trobat == NULL){
        echo "0 results";
    }else{
        echo $trobat;
    }
?>