<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>New Trade</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="theme.css">
        <script type="text/javascript" src="index.js"></script>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <link rel="shortcut icon" type="image/ico" href="img/Iconos/favicon.ico"/>
    </head>
    <body>
        <?php
        require_once 'login.php';
        require_once 'user.php';
        $user = new user;
        $steamid= '76561198007213978';
        if(isset($_POST['tradelink'])){
            // Creacio conexio a la base de dades
            $conn = new mysqli($servername, $username, $password, $database);
            // Comprobacio de la conexio
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
            
            $sql = 'UPDATE users SET tradelink = "'.$_POST['tradelink'].'" WHERE steamid = "'.$steamid.'";';
            
            if ($conn->query($sql) === TRUE) {
            } else {
                echo "Error updating record: " . $conn->error;
            }
            $conn->close;
        }
        ?>
        <br><center><div class="logo"><img id="logo" src="img/logoCSGOTrades.png" alt="logo"></div></center><br>
        <nav class="navbar navbar-default">
            <ul class="nav nav-pills">
                <li role="presentation" id="presentation"><a href="index.php">Home</a></li>
                <li role="presentation" id="presentation" class="active"><a href="newTrade.php">New Trade</a></li>
                <li role="presentation" id="presentation"><a href="searchTrades.php">Search</a></li>
                <li role="presentation" id="presentation"><a href="searchAllTrades.php">Search All</a></li>
                <li role="presentation" id="presentation"><a href="profile.php">Profile</a></li>
            </ul>
        </nav>
        <script>
            var steamid='<?php echo $steamid;?>';
        </script>
        <div class="container">
            <div class="row">
                <div class="col-md-12" id='notifications'>
                    <?php 
                        // Creacio conexio a la base de dades
                        $conn = new mysqli($servername, $username, $password, $database);
                        // Comprobacio de la conexio
                        if ($conn->connect_error) {
                            die("Connection failed: " . $conn->connect_error);
                        }
                        
                        $sql = "SELECT tradelink FROM users WHERE steamid='".$steamid."'";
                        
                        $result = $conn->query($sql);
                        
                        if ($result->num_rows > 0) {
                            // output data of each row
                            while($row = $result->fetch_assoc()) {
                                if($row['tradelink'] == null){
                                    echo "<center><form action='#' method='POST'><input type='text' name='tradelink'> <a href='http://steamcommunity.com/id/id/tradeoffers/privacy#trade_offer_acces_url'>Where to get?</a> <input type='Submit' value='Submit'><form></center>";   
                                }
                            }
                        }
                        $conn->close();
                    ?>
                </div>
                <div class="col-md-5" id='tradeUser'>
                    <div class="row itemsUserTrade">
                        <center><h1>Items User</h1></center>
                    </div>
                    <div class="row">
                        <div class='col-md-12' id='newTrade'>
                            <div class='row'>
                                <div class='col-md-12'>
                                    <span id="spanTrade"><button id="buttonNewTrade" onclick="newTrade()">New</button></span>
                                </div>
                            </div>
                            <div class='row'>
                                <div class='col-md-5' id='left'></div>
                                <div class='col-md-2'></div>
                                <div class='col-md-5' id='right'>
                                    <div class='row' id='rowItemsWeb'></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>    
                        <div class='col-md-12' id='itemsUser'>
                        <?php
                        $arrayItems = $user->GetUserInventory($steamid);
                        $item = file_get_contents('http://steamcommunity.com/profiles/'.$steamid.'/inventory/json/730/2');
                        $items = json_decode($item);
                            for($i=0;$i<sizeof($arrayItems);$i++){
                                $imgItem = $items->rgInventory->{$arrayItems[$i]}->classid.'_'.$items->rgInventory->{$arrayItems[$i]}->instanceid;
                              
                                if($items->rgDescriptions->$imgItem->tradable == 1){
                                    
                                    $text = $items->rgDescriptions->$imgItem->market_name;
                               
                                    $nueva_cadena = ereg_replace("[★]", "", $text);
                                    $nueva_cadena = ereg_replace("[™]", "", $nueva_cadena);
                                    list($tipus, $segon) = split('[|]', $nueva_cadena);
                                    $tipus = trim($tipus);
                                    list($skin, $tercer) = split('[(]', $segon);
                                    $skin = trim($skin);
                                    list($exterior) = split('[)]', $tercer);
                                    $exterior = trim($exterior);
                                    
                                    $linkImgItem = 'http://steamcommunity-a.akamaihd.net/economy/image/'.$items->rgDescriptions->$imgItem->icon_url;
                                    $find = 'StatTrak';
                                    $pos = strpos($tipus, $find);
                                    
                                    if($pos === false){
                
                                        echo '<div id="'.$imgItem.'" class="item" onclick="clickItemUser(this.id)">'.$tipus.'<br><img src='.$linkImgItem.'><br>'.$skin.'</div>';
                                    }else{
                                        echo '<div id="'.$imgItem.'" class="item stattrak" onclick="clickItemUser(this.id)">'.$tipus.'<br><img src='.$linkImgItem.'><br>'.$skin.'</div>';
                                    }
                                }
                            }
                        ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-2" id='mid'></div>
                <div class="col-md-5">
                    <div class="row itemsWebTrade">
                        <center><h1>Items Web</h1></center>
                    </div>
                    <div class='row'>
                        <div class='col-md-12 form'>
                            <form name='itemsForm' method="GET" onchange="FAjax('buscar.php','items','tipus='+document.getElementById('tipus').value+'&amp;nom='+document.getElementById('nom').value+'&amp;exterior='+document.getElementById('exterior').value,'POST');return false" action="#">
                                <select id='tipus'>
                                    <option value='all' selected>Type All</option>
                                    <option value='Knife'>Knife</option>
                                    <option value='Pistol'>Pistol</option>
                                    <option value='Shotgun'>Shotgun</option>
                                    <option value='SMG'>SMG</option>
                                    <option value='Rifle'>Rifle</option>
                                    <option value='Sniper Rifle'>Sniper Rifle</option>
                                    <option value='Machinegun'>Machinegun</option>
                                </select>
                                <select id='nom'>
                                    <option value='all' selected>Name All</option>
                                </select>
                                <select id='exterior'>
                                    <option value='all' selected>Exterior All</option>
                                    <option value='Factory New'>Factory New</option>
                                    <option value='Minimal Wear'>Minimal Wear</option>
                                    <option value='Field-Tested'>Field-Tested</option>
                                    <option value='Well-Worn'>Well-Worn</option>
                                    <option value='Battle-Scarred'>Battle-Scarred</option>
                                </select>
                            </form>
                        </div>
                    </div>
                    <div class='row'>
                        <div class='col-md-12' id='items'>
                     </div>
                </div>
            </div>
        </div>
        </div>
        </div>
        <div id="proba">aaaa</div>
        <script>
            var tipus = 'all';
            var nom = 'all';
            var exterior = 'all';
            FAjax('buscar.php','items','tipus='+tipus+'&nom='+nom+'&exterior='+exterior,'POST');
        </script>
    </body>
</html>