$(document).ready(function(){
    $('#tipus').change(function(){ 
        if($('#tipus option:selected').text() == 'Type All'){
            $('#nom').empty();
            $('#nom').append(new Option("Name all", "all"));
        }
        if($('#tipus option:selected').text() == 'Knife'){
            $('#nom').empty();
            $('#nom').append(new Option("Name all", "all"));
            $('#nom').append(new Option("Bayonet", "Bayonet"));
            $('#nom').append(new Option("Bowie Knife", "Bowie Knife"));
            $('#nom').append(new Option("Butterfly Knife", "Butterfly Knife"));
            $('#nom').append(new Option("Falchion Knife", "Falchion Knife"));
            $('#nom').append(new Option("Flip Knife", "Flip Knife"));
            $('#nom').append(new Option("Gut Knife", "Gut Knife"));
            $('#nom').append(new Option("Huntsman Knife", "Huntsman Knife"));
            $('#nom').append(new Option("Karambit", "Karambit"));
            $('#nom').append(new Option("M9 Bayonet", "M9 Bayonet"));
            $('#nom').append(new Option("Shadow Daggers", "Shadow Daggers"));
        }
        if($('#tipus option:selected').text() == 'Pistol'){
            $('#nom').empty();
            $('#nom').append(new Option("Name all", "all"));
            $('#nom').append(new Option("CZ75-Auto", "CZ75-Auto"));
            $('#nom').append(new Option("Desert Eagle", "Desert Eagle"));
            $('#nom').append(new Option("Dual Berettas", "Dual Berettas"));
            $('#nom').append(new Option("Five-SeveN", "Five-SeveN"));
            $('#nom').append(new Option("Glock-18", "Glock-18"));
            $('#nom').append(new Option("P2000", "P2000"));
            $('#nom').append(new Option("P250", "P250"));
            $('#nom').append(new Option("R8 Revolver", "R8 Revolver"));
        }
        if($('#tipus option:selected').text() == 'Shotgun'){
            $('#nom').empty();
            $('#nom').append(new Option("Name all", "all"));
            $('#nom').append(new Option("MAG-7", "MAG-7"));
            $('#nom').append(new Option("Nova", "Nova"));
            $('#nom').append(new Option("Sawed-Off", "Sawed-Off"));
        }
        if($('#tipus option:selected').text() == 'SMG'){
            $('#nom').empty();
            $('#nom').append(new Option("Name all", "all"));
            $('#nom').append(new Option("MAC-10", "MAC-10"));
            $('#nom').append(new Option("MP7", "MP7"));
            $('#nom').append(new Option("MP9", "MP9"));
            $('#nom').append(new Option("P90", "P90"));
            $('#nom').append(new Option("PP-Bizon", "PP-Bizon"));
        }
        if($('#tipus option:selected').text() == 'Rifle'){
            $('#nom').empty();
            $('#nom').append(new Option("Name all", "all"));
            $('#nom').append(new Option("AK-47", "AK-47"));
            $('#nom').append(new Option("AUG", "AUG"));
            $('#nom').append(new Option("FAMAS", "FAMAS"));
            $('#nom').append(new Option("Galil AR", "Galil AR"));
            $('#nom').append(new Option("M4A1-S", "M4A1-S"));
            $('#nom').append(new Option("M4A4", "M4A4"));
            $('#nom').append(new Option("SG 553", "SG 553"));
        }
        if($('#tipus option:selected').text() == 'Sniper Rifle'){
            $('#nom').empty();
            $('#nom').append(new Option("Name all", "all"));
            $('#nom').append(new Option("AWP", "AWP"));
            $('#nom').append(new Option("G3SG1", "G3SG1"));
            $('#nom').append(new Option("SCAR-20", "SCAR-20"));
        }
        if($('#tipus option:selected').text() == 'Machinegun'){
            $('#nom').empty();
            $('#nom').append(new Option("Name all", "all"));
            $('#nom').append(new Option("M249", "M249"));
            $('#nom').append(new Option("Negev", "Negev"));
        }
    })
});

var maxItemsWeb = 0;
var maxItemsUser = 0;

var ajax;
function creaAjax(){

        try {
    
        ajax=new ActiveXObject("MSXML2.XMLHTTP.3.0");
    
        }catch (e) {
    
        	try {
        	/*Per a donar més suport a la àmplia gamma de navegadors IE*/	
        	ajax=new ActiveXObject("Microsoft.XMLHTTP")	
        	
        	}
        	catch (E) {
        		ajax=false;
        	}
        
        
        }
        if (!ajax && typeof XMLHttpRequest!='undefined') {
        	ajax=new XMLHttpRequest();
        }
    return ajax;
    }
    
    function procesaAjax(){
        if(ajax.readyState==1){
            document.getElementById('items').innerHTML = 'carregant';
        }else if(ajax.readyState==4){
            if(ajax.status==200){
                var content=ajax.responseText;
                document.getElementById('items').innerHTML = content;
            }
            else if(ajax.status==404){
                document.getElementById('items').innerHTML = 'la adreça no existeix';
            }
            else{
                document.getElementById('items').innerHTML = ajax.status;
            }
        }
    }
    
    function FAjax(url,capa,valors,metode){
        creaAjax();
        if(ajax){
            ajax.open(metode,url+'?'+valors,true);
            ajax.onreadystatechange=procesaAjax;
            ajax.send(null);
            
        }
        else{
            alert("ERROR");
        }
    }
    
    function procesaNewTrade(){
        if(ajax.readyState==1){
            document.getElementById('notifications').innerHTML = 'carregant';
        }else if(ajax.readyState==4){
            if(ajax.status==200){
                var content=ajax.responseText;
                document.getElementById('notifications').innerHTML = content;
            }
            else if(ajax.status==404){
                document.getElementById('notifications').innerHTML = 'la adreça no existeix';
            }
            else{
                document.getElementById('notifications').innerHTML = ajax.status;
            }
        }
    }
    
    function ajaxTrade(url,valors,metode){
        creaAjax();
        if(ajax){
            ajax.open(metode,url+'?'+valors,true);
            ajax.onreadystatechange=procesaNewTrade;
            ajax.send(null);
            
        }
        else{
            alert("ERROR");
        }
    }
    
    function procesaNewSearch(){
        if(ajax.readyState==1){
            document.getElementById('notifications').innerHTML = 'carregant';
        }else if(ajax.readyState==4){
            if(ajax.status==200){
                var content=ajax.responseText;
                document.getElementById('itemsTrobats').innerHTML = content;
            }
            else if(ajax.status==404){
                document.getElementById('notifications').innerHTML = 'la adreça no existeix';
            }
            else{
                document.getElementById('notifications').innerHTML = ajax.status;
            }
        }
    }
    
    function ajaxSearch(url,valors,metode){
        creaAjax();
        if(ajax){
            ajax.open(metode,url+'?'+valors,true);
            ajax.onreadystatechange=procesaNewSearch;
            ajax.send(null);
            
        }
        else{
            alert("ERROR");
        }
    }
    
    function clickItemUser(id) {
        //Copia l'item seleccionat de l'usuari a la zona d'intercanvi
        //alert(id);
        if(maxItemsUser < 6){
            var div = document.getElementById(id),
            clone = div.cloneNode(true);
            clone.setAttribute("onClick","removeItemUser(this.id)");
            document.getElementById('left').appendChild(clone);
            maxItemsUser++;
        }
        else{
            alert("Maxim d'items d'usuari");
        }
    }
    function clickItemWeb(id) {
        //Copia l'item de la web a la zona d'intercanvi
        //alert(id);
        if(maxItemsWeb <6){
            var div = document.getElementById(id),
            clone = div.cloneNode(true); 
            
            var clase = clone.getAttribute("class");
            if(/stattrak/.test(clase)){
                clone.setAttribute("class", "col-md-8 col-md-offset-2 itemRowItems item stattrak");
                clone.setAttribute("onClick","removeItemWeb(this.id)");
                document.getElementById('rowItemsWeb').appendChild(clone);
                maxItemsWeb++;
            }else{
                clone.setAttribute("class", "col-md-8 col-md-offset-2 itemRowItems item");
                clone.setAttribute("onClick","removeItemWeb(this.id)");
                document.getElementById('rowItemsWeb').appendChild(clone);
                maxItemsWeb++;
            }
            
        }else{
            alert("Maxim d'items web");
        }
    }
    
    function removeItemUser(id){
        //treu l'item seleccionat de la zona d'intercanvi(part usuari
        var element = document.getElementById(id);
        element.parentNode.removeChild(element);
        maxItemsUser--;
    }
    
    function removeItemWeb(id){
        //treu l'item seleccionat de la zona d'intercanvi(part web)
        var element = document.getElementById(id);
        element.parentNode.removeChild(element);
        maxItemsWeb--;
    }
    
    function newTrade(){
        //Array d'items de la web i de l'usuari
        var arrayItemsWeb = [];
        var arrayItemsUser = [];
        
        //Agafem els items de la zona d'intercanvis(Part objectes de la web)
        var itemsWeb = document.getElementById("rowItemsWeb").children;
        //Agafem els items de la zona d'intercanvis(Part objectes de l'usuari)
        var itemsUser = document.getElementById("left").children;
        
        
        if(itemsWeb.length == 0 || itemsUser.length == 0){
            alert("Falten items a la zona d'intercanvi");
        }else{
            //Omplim l'array amb items de la web
            for(var i = 0; i < itemsWeb.length; i++) {
                arrayItemsWeb.push(itemsWeb[i].id);
            }
            //Omplim l'array amb els items de l'usuari
            for(var i = 0; i < itemsUser.length; i++) {
                arrayItemsUser.push(itemsUser[i].id);
            }
            //alert("Intercanvi Items User: "+arrayItemsUser+" Items Web: "+arrayItemsWeb);
            ajaxTrade('insertTrade.php','steamid='+steamid+'&itemsUser='+arrayItemsUser+'&itemsWeb='+arrayItemsWeb,'GET');
            document.getElementById("rowItemsWeb").innerHTML = "";
            document.getElementById("left").innerHTML = "";
            maxItemsWeb = 0;
            maxItemsUser = 0;
        }
    }
    
    function newSearch(){
        var arrayItemsSearch = [];
        //Agafem els items de la zona d'intercanvis(Part objectes de la web)
        var itemsWeb = document.getElementById("rowItemsWeb").children;
        
        if(itemsWeb.length == 0){
            alert("Falten items a la zona d'intercanvi");
        }else{
            //Omplim l'array amb items de la web
            for(var i = 0; i < itemsWeb.length; i++) {
                arrayItemsSearch.push(itemsWeb[i].id);
            }
            ajaxSearch('search.php','itemsBuscar='+arrayItemsSearch,'GET')
        }
        
    }