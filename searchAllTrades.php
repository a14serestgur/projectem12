<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Search All Trades</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="theme.css">
        <script type="text/javascript" src="index.js"></script>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <link rel="shortcut icon" type="image/ico" href="img/Iconos/favicon.ico"/>
    </head>
    <body>
        <?php
        require_once 'login.php';
        require_once 'user.php';
        $user = new user;
        $steamid= '76561198007213978';
        if(isset($_POST['tradelink'])){
            // Creacio conexio a la base de dades
            $conn = new mysqli($servername, $username, $password, $database);
            // Comprobacio de la conexio
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
            
            $sql = 'UPDATE users SET tradelink = "'.$_POST['tradelink'].'" WHERE steamid = "'.$steamid.'";';
            
            if ($conn->query($sql) === TRUE) {
            } else {
                echo "Error updating record: " . $conn->error;
            }
            $conn->close;
        }
        ?>
        <br><center><div class="logo"><img src="img/logoCSGOTrades.png" alt="logo"></div></center><br>
        <nav class="navbar navbar-default">
            <ul class="nav nav-pills">
                <li role="presentation" id="presentation"><a href="index.php">Home</a></li>
                <li role="presentation" id="presentation"><a href="newTrade.php">New Trade</a></li>
                <li role="presentation" id="presentation"><a href="searchTrades.php">Search</a></li>
                <li role="presentation" id="presentation" class="active"><a href="searchAllTrades.php">Search All</a></li>
                <li role="presentation" id="presentation"><a href="profile.php">Profile</a></li>
            </ul>
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-md-12" id='notifications'>
                    <?php 
                        // Creacio conexio a la base de dades
                        $conn = new mysqli($servername, $username, $password, $database);
                        // Comprobacio de la conexio
                        if ($conn->connect_error) {
                            die("Connection failed: " . $conn->connect_error);
                        }
                        
                        $sql = "SELECT tradelink FROM users WHERE steamid='".$steamid."'";
                        
                        $result = $conn->query($sql);
                        
                        if ($result->num_rows > 0) {
                            // output data of each row
                            while($row = $result->fetch_assoc()) {
                                if($row['tradelink'] == null){
                                    echo "<center><form action='#' method='POST'><input type='text' name='tradelink'> <a href='http://steamcommunity.com/id/id/tradeoffers/privacy#trade_offer_acces_url'>Where to get?</a> <input type='Submit' value='Submit'><form></center>";   
                                }
                            }
                        }
                        $conn->close;
                    ?>
                </div>
                <div class='col-md-12' id='items'>
                    <div class='row'>
                        <div class='col-md-10 col-md-offset-1' id='items'>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>
        <script>
            var tipus = 'all';
            var nom = 'all';
            var exterior = 'all';
            FAjax('searchAll.php','items','','POST');
        </script>
    </body>
</html>